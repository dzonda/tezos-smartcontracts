import { Bytes, Key, MichelineType,Address, Nat, Option, Or, pair_to_mich, Signature, string_to_mich, Entrypoint } from '@completium/archetype-ts-types'
import { blake2b, expect_to_fail, get_account, get_chain_id, pack, set_mockup, set_mockup_now, set_quiet } from '@completium/experiment-ts'

import { get_packed_transfer_params, get_transfer_permit_data, get_missigned_error, wrong_packed_transfer_params, wrong_sig } from './utils'

const assert = require('assert');

/* Contracts */

import { rbac } from './binding/rbac';
 
/* Accounts ----------------------------------------------------------------- */

const alice = get_account('alice');
const bob = get_account('bob');
const carl = get_account('carl');

/* Endpoint ---------------------------------------------------------------- */

set_mockup()

/* Verbose mode ------------------------------------------------------------ */

set_quiet(true);

/* Now --------------------------------------------------------------------- */

const now = new Date("2022-01-01")
set_mockup_now(now)


/* Scenarios --------------------------------------------------------------- */

describe('[RBAC] Contract deployment', async () => {
  it('RBAC contract deployment should succeed', async () => {
    await rbac.deploy(alice.get_address(), { as: alice })
  });
});

describe('[RBAC] RBAC functions', async () => {
  it('Add Role by owner should succeed', async () => {
    await rbac.addRole(new Nat('2'), {as: alice})
    await rbac.addBearer(carl.get_address(), new Nat('2'), {as: alice})
    const check_role = await rbac.view_hasRole(carl.get_address(), new Nat('2'), {as: alice})
    assert(check_role == true)
  });

  it('Add Bearer by non-owner should fail', async () => {
    await expect_to_fail(async () => {
      await rbac.addBearer(carl.get_address(), new Nat('0'), {as: carl})
    }, rbac.errors.INVALID_CALLER);
  });

  it('Remove Bearer for a user should succeed', async () => {
    await rbac.removeBearer(carl.get_address(), new Nat('2'), {as: alice})
    const check_role = await rbac.view_hasRole(carl.get_address(), new Nat('2'), {as: alice})
    assert(check_role == false)
  });

  it('Remove role should succeed', async () => {
    await rbac.addRole(new Nat('300'), {as: alice})
    await rbac.removeRole(new Nat('300'), {as: alice})
    await expect_to_fail(async () => {
      await rbac.addBearer(carl.get_address(), new Nat('300'), {as: alice})
    }, rbac.errors.ROLE_NOT_EXIST);

  });

  it('Add Bearer for removed role should fail', async () => {
    await expect_to_fail(async () => {
      await rbac.addBearer(carl.get_address(), new Nat('300'), {as: carl})
    }, rbac.errors.INVALID_CALLER);
  });

  it('Remove Bearer for non-hold role should fail', async () => {
    await rbac.addRole(new Nat('400'), {as: alice})
    await expect_to_fail(async () => {
      await rbac.removeBearer(carl.get_address(), new Nat('400'), {as: alice})
    }, rbac.errors.r5);
  });
  

});
